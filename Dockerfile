FROM python:2.7-alpine
LABEL maintainer="Sheshagiri Rao Mallipedhi <msheshagirirao@gmail.com>"
WORKDIR /

ADD src/app.py app.py
ADD src/Pipfile Pipfile
ADD src/Pipfile.lock Pipfile.lock

RUN pip install pipenv
RUN pipenv install

EXPOSE 8080

ENTRYPOINT ["pipenv","run","python","app.py"]