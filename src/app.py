from flask import Flask, json, jsonify, Response
from flask_cors import CORS

app = Flask(__name__)
CORS(app)

@app.route("/getBooks")
def getBooks():
    
    try:

        # Initialize a book list
        books = [{
                'id': 1,    
                'title': 'title one',
                'author': 'author one',
                'genre': 'genre one'
            },
            {
                'id': 2,    
                'title': 'title two',
                'author': 'author two',
                'genre': 'genre two'
            },
            {
                'id': 3,    
                'title': 'title three',
                'author': 'author three',
                'genre': 'genre three'
            },
            {
                'id': 4,    
                'title': 'title four',
                'author': 'author four',
                'genre': 'genre four'
            }]
    
        # convert to json data
        jsonStr = json.dumps(books)
        resp = Response(jsonStr, status=200, mimetype='application/json')

    except Exception ,e:
        print str(e)

    return resp

if __name__ == "__main__":
    app.run(host="0.0.0.0",port="8080",debug=True)